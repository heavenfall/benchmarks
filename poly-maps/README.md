A "Polygon Map" is a description of a 2D pathfinding environment where 
only the obstacles to be avoided are specified. This kind of input is
useful for a variety of algorithms, such as those based on Visibility Graphs.
