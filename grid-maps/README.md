In this directory: 

| Path | Description |
| :--- | :---------- |
├ [gppc-2013](https://bitbucket.org/shortestpathlab/benchmarks/src/master/grid-maps/gppc-2013/) (Mirror) Benchmarks from the 2013 Grid-based Path Planning Competition
├ [gppc-2014](https://bitbucket.org/shortestpathlab/benchmarks/src/master/grid-maps/gppc-2014/) (Mirror) Benchmarks from the 2014 Grid-based Path Planning Competition
├ [movingai](https://bitbucket.org/shortestpathlab/benchmarks/src/master/grid-maps/movingai/) (Mirror) Benchmarks from Nathan Sturtevant's [MovingAI](https://movingai.com/benchmarks/grids.html) repository.
└ [local](https://bitbucket.org/shortestpathlab/benchmarks/src/master/grid-maps/local/) (Mirror) Benchmarks from the [HOG2](https://github.com/nathansttt/hog2) pathfinding library.


