A navigation mesh is a convex decomposition of the traversable space in a
2D pathfinding environment. This type of input is popular with pathfinding 
algorithms intended for computer game and computer graphics applications.
